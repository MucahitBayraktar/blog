<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Admin\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('', DashboardController::class)->name('dashboard');

Route::prefix('users')
    ->name('users.')
    ->group(function () {
        Route::get('', [UsersController::class, 'index'])->name('index');
    });

Route::prefix('posts')
    ->name('posts.')
    ->group(function () {
        Route::get('', [PostsController::class, 'index'])->name('index');
        Route::get('create', [PostsController::class, 'create'])->name('create');
        Route::post('', [PostsController::class, 'store'])->name('store');
        Route::get('{post}/edit', [PostsController::class, 'edit'])->name('edit');
        Route::put('{post}', [PostsController::class, 'update'])->name('update');
        Route::get('{post}/delete', [PostsController::class, 'destroy'])->name('destroy');
    });
