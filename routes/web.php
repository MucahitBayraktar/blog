<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\RegisterController;
use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', HomeController::class)->name('home');

Route::middleware(RedirectIfAuthenticated::class)
    ->group(function () {
        Route::match(['get', 'post'], 'login', LoginController::class)->name('login');
        Route::match(['get', 'post'], 'register', RegisterController::class)->name('register');
    });

Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::prefix('posts')->name('posts.')->group(function () {
    Route::post('{post}/vote', [PostsController::class, 'vote'])->name('vote')->middleware('auth');
    Route::get('{post}', [PostsController::class, 'show'])->name('show');
});
