<?php

use App\Http\Controllers\API\PostsController;
use App\Http\Controllers\API\TokenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('token', TokenController::class)->name('token');

Route::middleware('auth:api')
    ->group(function () {
        Route::prefix('posts')->name('posts.')->group(function () {
            Route::get('', [PostsController::class, 'index'])->name('index');
            Route::post('', [PostsController::class, 'store'])->name('store');
            Route::put('{post}', [PostsController::class, 'update'])->name('update');
            Route::put('{post}/publish', [PostsController::class, 'publish'])->name('publish');
            Route::put('{post}/unpublish', [PostsController::class, 'unpublish'])->name('unpublish');
            Route::delete('{post}', [PostsController::class, 'destroy'])->name('destroy');
        });
    });
