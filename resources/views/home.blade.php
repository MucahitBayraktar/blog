@extends('layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            @foreach($posts as $post)
                <div class="blog-post">
                    <h2 class="blog-post-title">
                        <a href="{{route('posts.show',['post'=>$post->id])}}">{{$post->title}}</a>
                    </h2>
                    <p class="blog-post-meta">{{$post->createdAtWithFormat('M d, Y')}} by {{$post->user->fullName()}}</p>
                    {!! $post->content !!}
                </div>
            @endforeach
        </div>
    </div>
@endsection
