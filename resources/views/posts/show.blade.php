@extends('layout')

@section('title',$post->title)

@section('styles')
    <link rel="stylesheet" href="{{asset('css/blog.css')}}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="blog-post">
                <h2 class="blog-post-title">{{$post->title}}</h2>
                <p class="blog-post-meta">
                    {{$post->createdAtWithFormat('M d, Y')}} by {{$post->user->fullName()}}&mdash;
                    {{$post->rating}}/5 ({{$post->voters->count()}}) </p>
                {!! $post->content !!}
            </div>
            <div class="text-center">
                <form action="{{route('posts.vote',['post'=>$post->id])}}" method="POST">
                    <div class="form-group">
                        @for($i=1;$i<=5;$i++)
                            <label class="p-1"><input type="radio" name="vote" value="{{$i}}"> {{$i}}</label>
                        @endfor
                        <button type="submit" class="btn btn-sm btn-secondary">vote</button>
                    </div>
                </form>
            </div>
            <div class="text-center">
                @if($previous)
                    <div class="float-left">
                        <a href="{{route('posts.show',['post'=>$previous->id])}}">{{$previous->title}}</a>
                    </div>
                @endif
                @if($next)
                    <div class="float-right">
                        <a href="{{route('posts.show',['post'=>$next->id])}}">{{$next->title}}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
