<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>login &mdash; blog</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
</head>
<body class="text-center">

<form class="form-signin" action="{{route('login')}}" method="POST">
    @csrf
    <h1 class="h3 mb-3 font-weight-normal">
        <a href="{{route('home')}}">blog</a>
    </h1>
    @if(session('error'))
        <div class="alert alert-warning mb-3">
            {{session('error')}}
        </div>
    @endif
    <div class="form-group">
        <label for="inputEmail" class="sr-only">email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="email" required>
    </div>
    <div class="form-group">
        <label for="inputPassword" class="sr-only">password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="password" required>
    </div>
    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" type="submit">login</button>
    </div>
    <div class="form-group">
        <a href="{{route('register')}}">register</a>
    </div>
</form>


</body>
</html>
