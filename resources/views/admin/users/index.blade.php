@extends('admin.layout')

@section('title','users')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">users</h1>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-sm table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>name</th>
                <th>email</th>
                <th>roles</th>
                <th>created at</th>
                <th>updated at</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->fullName()}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if($user->isAdmin())
                            <label class="badge badge-danger">admin</label>
                        @endif
                        @if($user->isModerator())
                            <label class="badge badge-warning">moderator</label>
                        @endif
                        @if($user->isWriter())
                            <label class="badge badge-info">writer</label>
                        @endif
                    </td>
                    <td>{{$user->createdAtWithFormat('d.m.Y H:i:s')}}</td>
                    <td>{{$user->updatedAtWithFormat('d.m.Y H:i:s')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
