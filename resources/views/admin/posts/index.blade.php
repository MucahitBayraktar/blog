@extends('admin.layout')

@section('title','posts')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">posts</h1>
        <a href="{{route('admin.posts.create')}}">create post</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-sm table-hover">
            <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                @if(auth()->user()->isAdmin())
                    <th>writer</th>
                @endif
                <th>status</th>
                <th>created at</th>
                <th>updated at</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    @if(auth()->user()->isAdmin())
                        <td>{{$post->user->fullName()}}</td>
                    @endif
                    <td>
                        <label class="{{$post->isActive() ? 'text-success':'text-secondary'}}">
                            {{$post->isActive() ? 'active':'passive'}}
                        </label>
                    </td>
                    <td>
                        {{$post->createdAtWithFormat('d.m.Y H:i:s')}}
                    </td>
                    <td>
                        {{$post->updatedAtWithFormat('d.m.Y H:i:s')}}
                    </td>
                    <td>
                        <a href="{{route('admin.posts.edit',['post'=>$post->id])}}">
                            edit
                        </a>
                        <a href="{{route('admin.posts.destroy',['post'=>$post->id])}}" onclick="return confirm('are you sure?')" class="text-danger">
                            delete
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
