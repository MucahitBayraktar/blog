@extends('admin.layout')

@section('title','edit post')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">edit post</h1>
    </div>
    <form action="{{route('admin.posts.update',['post'=>$post->id])}}" method="POST">
        @method('PUT')
        <div class="form-group">
            <label>title</label> <input type="text" name="title" class="form-control" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <label>content</label> <textarea name="content" rows="10" class="form-control">{{$post->content}}</textarea>
        </div>
        <div class="form-group">
            <label><input type="checkbox" value="1" name="publish" id="postPublish" {{$post->publish_at ? 'checked':''}}> publish in the future
                <input type="datetime-local" name="publish_date" autocomplete="off" value="{{$post->publish_at?$post->publish_at->toDateTimeLocalString():''}}"></label>
        </div>
        <div class="form-group" id="postActiveWrapper" style="display: {{$post->publish_at ? 'none':'block'}};">
            <label><input type="checkbox" value="1" name="active" {{$post->isActive() ? 'checked':''}}> active</label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-warning">edit</button>
        </div>
    </form>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#postPublish').click(function () {
                $('#postActiveWrapper').toggle();
            });
        });
    </script>
@endsection
