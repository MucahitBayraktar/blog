@extends('admin.layout')

@section('title','create post')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">create post</h1>
    </div>
    <form action="{{route('admin.posts.store')}}" method="POST">
        <div class="form-group">
            <label>title</label> <input type="text" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label>content</label> <textarea name="content" rows="10" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label><input type="checkbox" value="1" name="publish" id="postPublish"> publish in the future
                <input type="datetime-local" name="publish_date" autocomplete="off"></label>
        </div>
        <div class="form-group" id="postActiveWrapper" style="display: block;">
            <label><input type="checkbox" value="1" name="active"> active</label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">save</button>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#postPublish').click(function () {
                $('#postActiveWrapper').toggle();
            });
        });
    </script>
@endsection
