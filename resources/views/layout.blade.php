<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @hasSection('title')
            @yield('title') &mdash; blog
        @else
            blog
        @endif
    </title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    @yield('styles')
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @auth
                    @if(auth()->user()->isPanelUser())
                        <li class="nav-item">
                            <a href="{{route('admin.dashboard')}}" class="nav-link">admin</a>
                        </li>
                    @endif
                @endauth
            </ul>
            <div class="my-2 my-lg-0">
                @auth
                    {{auth()->user()->fullName()}}
                    <a href="{{route('logout')}}">log out</a>
                @endauth
                @guest
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{route('login')}}" class="nav-link">login</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('register')}}" class="nav-link">register</a>
                        </li>
                    </ul>
                @endguest
            </div>
        </div>
    </div>
</nav>
<div class="container mt-3 mb-3">
    @yield('content')
</div>
<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
</body>
</html>
