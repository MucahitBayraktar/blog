<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::where(function ($query) {
                $query->where('is_admin', TRUE)
                    ->orWhere('is_writer', TRUE);
            })->inRandomOrder()->first()->id,
            'title' => $this->faker->text(32),
            'content' => $this->faker->text(512),
            'is_active' => rand(0, 1),
        ];
    }
}
