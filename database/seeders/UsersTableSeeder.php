<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('users')) {
            if (User::where('email', 'admin@mobillium.com')->exists() == FALSE) {
                User::create([
                    'name' => 'admin',
                    'surname' => 'admin',
                    'email' => 'admin@mobillium.com',
                    'password' => bcrypt('mobillium'),
                    'is_admin' => TRUE,
                    'is_moderator' => TRUE,
                    'is_writer' => TRUE,
                ]);
            }

            if (User::where('email', 'writer1@mobillium.com')->exists() == FALSE) {
                User::create([
                    'name' => 'writer1',
                    'surname' => 'writer1',
                    'email' => 'writer1@mobillium.com',
                    'password' => bcrypt('mobillium'),
                    'is_writer' => TRUE,
                ]);
            }
        }
    }
}
