# Blog

Install PHP packages

````shell
composer install
````

Create .env from .env.exmaple

````shell
cp .env.example .env
````

Create new key

````shell
php artisan key:generate
````

Set up database settings

````dotenv
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=blog
DB_USERNAME=
DB_PASSWORD=
````

Run database migration

````shell
php artisan migrate
````

Run database seeder

````shell
php artisan db:seed
````

Create passport encryption keys

````shell
php artisan passport:key
````

Create personal access client

````shell
php artisan passport:client --personal
````

Set up laravel passport personal settings

````dotenv
PASSPORT_PERSONAL_ACCESS_CLIENT_ID="client-id-value"
PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET="unhashed-client-secret-value"
````

Run schedule for scheduled publish post

````shell
php artisan schedule:run
````

Run project

````shell
php artisan serve
````
