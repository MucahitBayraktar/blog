<?php

namespace App\Traits;

trait TimestampsFormat
{
    public function createdAtWithFormat($format)
    {
        return $this->created_at->format($format);
    }

    public function updatedAtWithFormat($format)
    {
        return $this->updated_at->format($format);
    }
}
