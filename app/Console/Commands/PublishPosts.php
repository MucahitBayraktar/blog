<?php

namespace App\Console\Commands;

use App\Models\Post;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PublishPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish Posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $posts = Post::where('is_active', FALSE)
                ->whereNotNull('publish_at')
                ->where(function ($query) {
                    $query->where('publish_at', now())
                        ->orWhere('publish_at', '<=', now());
                })
                ->get();
            foreach ($posts as $post) {
                $post->publish_at = NULL;
                $post->is_active = TRUE;
                $post->save();
            }
        } catch (Exception $exception) {
            Log::warning($exception->getMessage());
        }
        return 0;
    }
}
