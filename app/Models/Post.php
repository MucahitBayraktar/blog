<?php

namespace App\Models;

use App\Scopes\OrderByDesc;
use App\Traits\TimestampsFormat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Whtht\PerfectlyCache\Traits\PerfectlyCachable;

class Post extends Model
{
    use HasFactory, PerfectlyCachable, TimestampsFormat;

    protected $fillable = [
        'user_id',
        'title',
        'content',
        'is_active',
    ];

    protected $casts = [
        'publish_at' => 'datetime',
    ];

    public static function booted()
    {
        parent::booted();
        static::addGlobalScope(new OrderByDesc());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isActive()
    {
        return $this->is_active;
    }

    public function previous()
    {
        return static::where('id', '<', $this->id)
            ->where('is_active', TRUE)
            ->orderByDesc('id')
            ->first();
    }

    public function next()
    {
        return static::where('id', '>', $this->id)
            ->where('is_active', TRUE)
            ->orderBy('id')
            ->first();
    }

    public function voters()
    {
        return $this->belongsToMany(User::class, 'votes', 'post_id', 'user_id', 'id', 'id')
            ->withPivot('vote')
            ->withTimestamps();
    }

    public function getRatingAttribute()
    {
        $voters = $this->voters;
        $rating = 0;
        $voteCount = $voters->count();
        foreach ($voters as $voter) {
            $rating += $voter->pivot->vote;
        }
        if ($rating) {
            return number_format($rating / $voteCount, 2, ',', '.');
        }
        return 0;
    }
}
