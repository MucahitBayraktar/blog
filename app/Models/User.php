<?php

namespace App\Models;

use App\Scopes\OrderByDesc;
use App\Traits\TimestampsFormat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, TimestampsFormat, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
        'is_moderator',
        'is_writer',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function isModerator()
    {
        return $this->is_moderator;
    }

    public function isWriter()
    {
        return $this->is_writer;
    }

    public function isPanelUser()
    {
        if ($this->is_admin | $this->is_moderator | $this->is_writer) {
            return TRUE;
        }
        return FALSE;
    }

    public function fullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function checkVote(int $postId)
    {
        return $this->votes()
            ->withoutGlobalScope(OrderByDesc::class)
            ->where('votes.post_id', $postId)->exists();
    }

    public function votes()
    {
        return $this->belongsToMany(Post::class, 'votes', 'user_id', 'post_id', 'id', 'id')
            ->withPivot('vote')
            ->withTimestamps();
    }
}
