<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function index()
    {
        $posts = [];
        if (Auth::user()->isAdmin()) {
            $posts = Post::paginate();
        } else {
            $posts = Auth::user()->posts()->paginate();
        }
        return PostResource::collection($posts);
    }

    public function store(Request $request)
    {
        $newPost = new Post();
        $newPost->user_id = Auth::id();
        $newPost->title = $request->input('title');
        $newPost->content = $request->input('content');
        $newPost->is_active = (int)$request->input('active');
        $newPost->save();
        return new PostResource($newPost);
    }

    public function update(Request $request, Post $post)
    {
        try {
            $this->authorize('update', $post);
            $post->user_id = Auth::id();
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->is_active = (int)$request->input('active');
            $post->save();
            return new PostResource($post);
        } catch (AuthorizationException $e) {
            return response(NULL, 403);
        }
    }

    public function publish(Post $post)
    {
        try {
            $this->authorize('update', $post);
            $post->is_active = TRUE;
            return new PostResource($post);
        } catch (AuthorizationException $e) {
            return response(NULL, 403);
        }
    }

    public function unpublish(Post $post)
    {
        try {
            $this->authorize('update', $post);
            $post->is_active = FALSE;
            return new PostResource($post);
        } catch (AuthorizationException $e) {
            return response(NULL, 403);
        }
    }

    public function destroy(Post $post)
    {
        try {
            $this->authorize('delete', $post);
            $post->delete();
        } catch (AuthorizationException $e) {
            return response(NULL, 403);
        }

    }


}
