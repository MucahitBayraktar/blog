<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        if (Auth::attempt($request->only(['email', 'password']))) {
            if (Auth::user()->isPanelUser()) {
                return response()->json(['access_token' => Auth::user()->createToken('API')->accessToken]);
            }
        }
        return response(NULL, 403);
    }
}
