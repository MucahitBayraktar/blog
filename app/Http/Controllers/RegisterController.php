<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        if ($request->isMethod('post')) {
            $newUser = new User();
            $newUser->name = $request->input('name');
            $newUser->surname = $request->input('surname');
            $newUser->email = $request->input('email');
            $newUser->password = bcrypt($request->input('password'));
            try {
                $newUser->saveOrFail();
                Auth::login($newUser);
                return redirect()->route('home');
            } catch (\Throwable $e) {
                Log::warning($e->getMessage());
                redirect()->back()->with([
                    'error' => 'Please try again later.',
                ]);
            }
        }
        return view('register');
    }
}
