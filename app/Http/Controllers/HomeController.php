<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $data = [];
        $data['posts'] = Post::with(['user'])
            ->where('is_active', TRUE)
            ->get();
        return view('home', $data);
    }
}
