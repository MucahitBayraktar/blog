<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::attempt($request->only('email', 'password'))) {
                if (Auth::user()->isPanelUser()) {
                    return redirect()->route('admin.dashboard');
                }
                return redirect()->route('home');
            } else {
                redirect()->back()->with([
                    'error' => 'Please try again later.',
                ]);
            }
        }
        return view('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
