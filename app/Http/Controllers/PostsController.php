<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function show(Post $post)
    {
        $data = [];
        $data['post'] = $post;
        $data['previous'] = $post->previous();
        $data['next'] = $post->next();
        return view('posts.show', $data);
    }

    public function vote(Request $request, Post $post)
    {
        if (Auth::user()->checkVote($post->id) == FALSE) {
            Auth::user()->votes()->attach($post, ['vote' => $request->input('vote')]);
        }
        return redirect()->route('posts.show', ['post' => $post->id]);
    }
}
