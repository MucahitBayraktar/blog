<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Post::class);

        $data = [];
        $posts = NULL;
        if (Auth::user()->isAdmin()) {
            $posts = Post::with(['user'])->skipCache()->get();
        } else {
            $posts = Auth::user()->posts()->with(['user'])->skipCache()->get();
        }
        $data['posts'] = $posts;
        return view('admin.posts.index', $data);
    }

    public function create()
    {
        $this->authorize('create', Post::class);

        return view('admin.posts.create');
    }

    public function store(Request $request)
    {
        $newPost = new Post();
        $newPost->user_id = Auth::id();
        $newPost->title = $request->input('title');
        $newPost->content = $request->input('content');
        if ($request->input('publish')) {
            $newPost->publish_at = $request->input('publish_date');
        } else if ($request->input('active')) {
            $newPost->is_active = TRUE;
        }
        $newPost->save();

        return redirect()->route('admin.posts.index');
    }

    public function edit(Request $request, Post $post)
    {
        if ($request->user()->cannot('update', $post)) {
            abort(403);
        }
        $data = [];
        $data['post'] = $post;
        return view('admin.posts.edit', $data);
    }

    public function update(Request $request, Post $post)
    {
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        if ($request->input('publish')) {
            $post->publish_at = $request->input('publish_date');
            $post->is_active = FALSE;
        } else if ($request->input('active')) {
            $post->is_active = TRUE;
            $post->publish_at = NULL;
        } else {
            $post->is_active = FALSE;
            $post->publish_at = NULL;
        }
        $post->save();

        return redirect()->route('admin.posts.index');
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $post->delete();
        return redirect()->route('admin.posts.index');
    }
}
